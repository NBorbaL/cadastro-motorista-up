package rblstudios.com.cadastromotorista.model;

import android.content.Context;

import rblstudios.com.cadastromotorista.R;

public class CategoriaMotorista {

    public static final int NORMAL = 0;
    public static final int PREMIUM = 1;

    public static String getCategoriaName(int id, Context ctx) {
        switch (id) {
            case NORMAL:
                return ctx.getString(R.string.categoria_motorista_normal);
            case PREMIUM:
                return ctx.getString(R.string.categoria_motorista_premium);
            default:
                return ctx.getString(R.string.categoria_motorista_nao_encontrada);
        }
    }

    public static int buscarCategoriaValorVeiculo(double valorVeiculo) {
        return valorVeiculo < 100000 ? NORMAL : PREMIUM;
    }
}
