package rblstudios.com.cadastromotorista.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import rblstudios.com.cadastromotorista.R;
import rblstudios.com.cadastromotorista.controller.MotoristaController;
import rblstudios.com.cadastromotorista.interfaces.FragmentoCallback;
import rblstudios.com.cadastromotorista.model.CategoriaMotorista;
import rblstudios.com.cadastromotorista.model.Motorista;
import rblstudios.com.cadastromotorista.model.Veiculo;
import rblstudios.com.cadastromotorista.util.ViewUtil;

/**
 * Criado por renan.lucas em 12/10/2017.
 */

public class CadastroMotoristaDadosBasicos extends Fragment {

    // Padrão é inclusão
    private boolean isInclusao = true;
    private FragmentoCallback callback;

    private EditText etNomeMotorista, etIdadeMotorista, etValidadeCNH, etTelefone, etPlacaVeiculo, etValorVeiculo;
    private TextView txtCategoriaMotorista;
    private RadioButton rbVeiculoLocado, rbVeiculoProprio;
    private Button btnSalvarMotorista;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cadastro_motorista_dados_basicos, container, false);

        //Acha componentes da tela por ID
        encontrarViewsPorId(rootView);

        // Verifica se algum dado veio via intent para alteração
        verificaIntentAlteracao();

        definirListenerCampos();
        definirListenerBotoes();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Implementa o callback
        try {
            callback = (FragmentoCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + getString(R.string.erro_implementacaoclasse, "ComErroDeValidacao"));
        }
    }

    private void encontrarViewsPorId(View rootView) {
        etNomeMotorista = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etNomeMotorista);
        etIdadeMotorista = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etIdadeMotorista);
        etTelefone = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etTelefone);
        etValidadeCNH = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etValidadeCarteiraMotorista);
        etPlacaVeiculo = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etPlacaVeiculo);
        rbVeiculoLocado = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_rbVeiculoLocado);
        rbVeiculoProprio = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_rbVeiculoProprio);
        etValorVeiculo = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_etValorVeiculo);
        txtCategoriaMotorista = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_txtCategoriaMotorista);
        btnSalvarMotorista = rootView.findViewById(R.id.CadastroMotoristaDadosBasico_btnSalvarMotorista);
    }

    private void verificaIntentAlteracao() {
        if (getActivity() != null) {
            if (getActivity().getIntent().hasExtra("idMotorista")) {
                int idMotoristaAlteracao = getActivity().getIntent().getIntExtra("idMotorista", 0);
                MotoristaController motoristaController = new MotoristaController(getContext());
                Motorista motoristaAlteracao = motoristaController.listarPorId(idMotoristaAlteracao);

                if (motoristaAlteracao.getNome() != null && !motoristaAlteracao.getNome().isEmpty()) {
                    etNomeMotorista.setText(motoristaAlteracao.getNome());
                }

                if (motoristaAlteracao.getIdade() > 0) {
                    etIdadeMotorista.setText(String.valueOf(motoristaAlteracao.getIdade()));
                }

                if (motoristaAlteracao.getTelefone() != null && !motoristaAlteracao.getTelefone().isEmpty()) {
                    etTelefone.setText(motoristaAlteracao.getTelefone());
                }

                if (motoristaAlteracao.getValidadeCNH() != null && !motoristaAlteracao.getValidadeCNH().isEmpty()) {
                    etValidadeCNH.setText(motoristaAlteracao.getValidadeCNH());
                }

                if (motoristaAlteracao.getPlacaVeiculo() != null && !motoristaAlteracao.getPlacaVeiculo().isEmpty()) {
                    etPlacaVeiculo.setText(motoristaAlteracao.getPlacaVeiculo());
                }

                switch (motoristaAlteracao.getTipoVeiculo()) {
                    case Veiculo.VEICULO_PROPRIO:
                        rbVeiculoProprio.setChecked(true);
                        break;
                    case Veiculo.VEICULO_LOCADO:
                        rbVeiculoLocado.setChecked(true);
                        break;
                }

                if (motoristaAlteracao.getValorVeiculo() > 0) {
                    etValorVeiculo.setText(String.valueOf(motoristaAlteracao.getValorVeiculo()));
                }

                int categoriaAtual = CategoriaMotorista.buscarCategoriaValorVeiculo(motoristaAlteracao.getValorVeiculo());
                txtCategoriaMotorista.setText(getString(R.string.CadastroMotoristaDadosBasicos_categoria,
                        CategoriaMotorista.getCategoriaName(categoriaAtual, getContext())));

                // Mudamos para alteração
                isInclusao = false;
                btnSalvarMotorista.setText(getString(R.string.botaoAlterar));
            }
        }
    }

    private void definirListenerCampos() {
        if (getActivity() != null) {
            etValorVeiculo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        Double valorVeiculo = Double.parseDouble(editable.toString());
                        int categoriaAtual = CategoriaMotorista.buscarCategoriaValorVeiculo(valorVeiculo);
                        txtCategoriaMotorista.setText(getString(R.string.CadastroMotoristaDadosBasicos_categoria,
                                CategoriaMotorista.getCategoriaName(categoriaAtual, getContext())));
                    } catch (Exception ex) {
                        txtCategoriaMotorista.setText(getString(R.string.categoria_motorista_nao_encontrada));
                    }
                }
            });
        }
    }

    private void definirListenerBotoes() {
        btnSalvarMotorista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    salvarMotorista(retornaObjetoMotorista());
                    mostraAlerta(isInclusao);
                } catch (Exception ex) {
                    ViewUtil.criaEMostraAlert(getContext(),
                            getString(R.string.title_erro),
                            ex.getMessage().trim(),
                            getString(R.string.botaoOK), true);
                }
            }
        });
    }

    private Motorista retornaObjetoMotorista() throws Exception {
        if (getActivity() != null) {
            Motorista motorista = new Motorista();
            if (getActivity().getIntent().hasExtra("idMotorista")) {
                motorista.setId(getActivity().getIntent().getIntExtra("idMotorista", 0));
            }

            if (!etNomeMotorista.getText().toString().trim().isEmpty()) {
                motorista.setNome(etNomeMotorista.getText().toString().trim());
            } else {
                throw new Exception("Nome do motorista não pode ser vazio.");
            }

            try {
                motorista.setIdade(Integer.valueOf(etIdadeMotorista.getText().toString().trim()));
            } catch (Exception ex) {
                throw new Exception("Idade do motorista inválida.");
            }

            if (!etTelefone.getText().toString().trim().isEmpty()) {
                motorista.setTelefone(etTelefone.getText().toString().trim());
            } else {
                throw new Exception("Telefone não pode ser vazio.");
            }

            if (!etValidadeCNH.getText().toString().trim().isEmpty()) {
                motorista.setValidadeCNH(etValidadeCNH.getText().toString().trim());
            } else {
                throw new Exception("Data de validade da CNH não pode ser vazia.");
            }

            if (!etPlacaVeiculo.getText().toString().trim().isEmpty()) {
                motorista.setPlacaVeiculo(etPlacaVeiculo.getText().toString().trim().toUpperCase());
            } else {
                throw new Exception("Placa do veículo não pode ser vazia.");
            }

            if (!rbVeiculoProprio.isChecked() && !rbVeiculoLocado.isChecked()) {
                throw new Exception("Tipo de veículo não marcado.");
            } else {
                motorista.setTipoVeiculo(rbVeiculoProprio.isChecked() ? Veiculo.VEICULO_PROPRIO : Veiculo.VEICULO_LOCADO);
            }

            try {
                motorista.setValorVeiculo(Double.parseDouble(etValorVeiculo.getText().toString().trim()));
            } catch (Exception ex) {
                throw new Exception("Valor do veículo inválido.");
            }

            motorista.setCategoriaMotorista(CategoriaMotorista.buscarCategoriaValorVeiculo(motorista.getValorVeiculo()));
            return motorista;
        }
        return null;
    }


    private void salvarMotorista(Motorista motorista) throws Exception {
        MotoristaController motoristaController = new MotoristaController(getContext());
        motoristaController.salvar(motorista);
    }


    private void mostraAlerta(boolean isInclusao) {
        if (getActivity() != null && getContext() != null) {
            // Constroi o alerta
            AlertDialog.Builder construtorAlerta = new AlertDialog.Builder(getContext());
            if (isInclusao) {
                construtorAlerta.setMessage(getString(R.string.mensagem_motoristacadastradosucesso));
            } else {
                construtorAlerta.setMessage(getString(R.string.mensagem_motoristaalteradosucesso));
            }
            construtorAlerta.setCancelable(true);

            construtorAlerta.setPositiveButton(
                    getString(R.string.botaoOK),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().finish();
                        }
                    });

            AlertDialog alerta = construtorAlerta.create();
            alerta.show();
        }
    }
}