package rblstudios.com.cadastromotorista.adapter;

import android.view.View;

/**
 * Criado por renan.lucas on 10/11/2017.
 */

public interface RecyclerViewClickListener {
    void onItemClick(View v, int position);
}
