package rblstudios.com.cadastromotorista.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import rblstudios.com.cadastromotorista.R;
import rblstudios.com.cadastromotorista.controller.MotoristaController;
import rblstudios.com.cadastromotorista.model.CategoriaMotorista;
import rblstudios.com.cadastromotorista.model.Motorista;

/**
 * Criado por renan.lucas on 10/11/2017.
 */

public class AdapterMotoristasList extends RecyclerView.Adapter<AdapterMotoristasList.ViewHolder> {

    private static final int VIEW_TYPE_NORMAL = 1;
    private static final int VIEW_TYPE_VAZIO = 2;

    private List<Motorista> motoristas;
    private RecyclerViewClickListener recyclerViewClickListener;
    private Context ctx;

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtNomeMotorista;
        private TextView txtCategoriaMotorista;
        private Button btnExcluir;

        private ViewHolder(View itemView) {
            super(itemView);
            this.txtNomeMotorista = itemView.findViewById(R.id.ItemListaMotorista_nomeMotorista);
            this.txtCategoriaMotorista = itemView.findViewById(R.id.ItemListaMotorista_categoriaMotorista);
            this.btnExcluir = itemView.findViewById(R.id.ItemListaMotorista_btnExcluir);
        }
    }

    public AdapterMotoristasList(Context ctx, List<Motorista> motoristas, RecyclerViewClickListener clickListener) {
        this.ctx = ctx;
        this.motoristas = motoristas;
        this.recyclerViewClickListener = clickListener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        final ViewHolder viewHolder;
        if (viewType == VIEW_TYPE_NORMAL) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_motorista, parent, false);
            viewHolder = new ViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewClickListener.onItemClick(v, viewHolder.getAdapterPosition());
                }
            });

            viewHolder.btnExcluir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    Motorista motoristaSelecionada = motoristas.get(viewHolder.getAdapterPosition());
                                    MotoristaController motoristaController = new MotoristaController(ctx);
                                    motoristaController.excluir(motoristaSelecionada.getId());
                                    motoristas.remove(viewHolder.getAdapterPosition());
                                    notifyItemRemoved(viewHolder.getAdapterPosition());
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setMessage(ctx.getString(R.string.mensagem_confirmacaoexclusao)).setPositiveButton(ctx.getString(R.string.botaoSim), dialogClickListener)
                            .setNegativeButton(ctx.getString(R.string.botaoNao), dialogClickListener).show();
                }
            });
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_motorista_vazio, parent, false);
            viewHolder = new ViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        if (viewType == VIEW_TYPE_NORMAL) {
            TextView txtNomeMotorista = holder.txtNomeMotorista;
            TextView txtCategoriaMotorista = holder.txtCategoriaMotorista;
            txtNomeMotorista.setText(motoristas.get(position).getNome());
            txtCategoriaMotorista.setText(CategoriaMotorista.getCategoriaName(motoristas.get(position).getCategoriaMotorista(), ctx));
        }
    }

    @Override
    public int getItemCount() {
        if (motoristas.size() == 0) {
            return 1;
        } else {
            return motoristas.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (motoristas.size() == 0) {
            return VIEW_TYPE_VAZIO;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }
}