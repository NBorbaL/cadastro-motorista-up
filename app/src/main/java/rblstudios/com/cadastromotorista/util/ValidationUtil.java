package rblstudios.com.cadastromotorista.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ValidationUtil {

    private static final String VALID_DATE_FORMAT = "dd/MM/yyyy";

    public static boolean isTelefone(String numeroTelefone) {
        return numeroTelefone.matches("^\\([1-9]{2}\\)(?:[2-8]|9[1-9])[0-9]{3}-[0-9]{4}$");
    }

    public static boolean isAgeValid(int age) {
        return age >= 18 && age <= 65;
    }

    public static boolean isValidCNH(Date dataValidade) throws Exception {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 1);
            Date validDate = calendar.getTime();
            return !dataValidade.before(validDate);
        } catch (Exception ex) {
            throw new Exception("Erro ao validar validade de CNH (" + ex.getMessage() + ")");
        }
    }

    public static boolean isValidCNH(String dataValidade) throws Exception {
        try {
            DateFormat dataValidadeDate = new SimpleDateFormat(VALID_DATE_FORMAT);
            dataValidadeDate.setLenient(false);
            dataValidadeDate.parse(dataValidade);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 1);
            Date validDate = calendar.getTime();
            Date cnhDate = dataValidadeDate.getCalendar().getTime();
            return !cnhDate.before(validDate);
        } catch (Exception ex) {
            throw new Exception("Erro ao validar validade de CNH (" + ex.getMessage() + ")");
        }
    }

    public static boolean isValidDate(String date) {
        try {
            DateFormat df = new SimpleDateFormat(VALID_DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean isValidPlaca(String placa) {
        return placa.matches("^[a-zA-Z]{3}-\\d{4}$");
    }
}
