package rblstudios.com.cadastromotorista.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import rblstudios.com.cadastromotorista.R;
import rblstudios.com.cadastromotorista.adapter.AdapterMotoristasList;
import rblstudios.com.cadastromotorista.adapter.RecyclerViewClickListener;
import rblstudios.com.cadastromotorista.controller.MotoristaController;
import rblstudios.com.cadastromotorista.model.Motorista;

public class ListagemMotoristas extends AppCompatActivity {

    private FloatingActionButton btnCadastrarMotoristas;
    private RecyclerView recyclerMotoristas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_motoristas);
        encontrarViewsPorId();
        criaListenerBotao();
    }

    @Override
    protected void onResume() {
        super.onResume();
        criaListMotoristas();
    }

    private void encontrarViewsPorId() {
        btnCadastrarMotoristas = findViewById(R.id.ListagemMotorista_btnCadastrarMotorista);
        recyclerMotoristas = findViewById(R.id.ListagemMotoristas_RecyclerViewMotoristas);
    }

    private void criaListMotoristas() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerMotoristas.setHasFixedSize(true);
        recyclerMotoristas.setLayoutManager(layoutManager);
        recyclerMotoristas.setItemAnimator(new DefaultItemAnimator());

        final MotoristaController motoristaController = new MotoristaController(this);
        final List<Motorista> motoristas = motoristaController.listarTudo();

        final AdapterMotoristasList adapterMotoristasList = new AdapterMotoristasList(ListagemMotoristas.this, motoristas, new RecyclerViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                // ALTERAÇÃO
                Motorista motoristaSelecionada = motoristas.get(position);
                Motorista motoristaBanco = motoristaController.listarPorId(motoristaSelecionada.getId());

                Intent intent = new Intent(ListagemMotoristas.this, CadastroMotorista.class);
                intent.putExtra("idMotorista", motoristaBanco.getId());
                startActivity(intent);
            }
        });

        recyclerMotoristas.setAdapter(adapterMotoristasList);
    }

    private void criaListenerBotao() {
        btnCadastrarMotoristas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListagemMotoristas.this, CadastroMotorista.class);
                startActivity(intent);
            }
        });
    }
}
