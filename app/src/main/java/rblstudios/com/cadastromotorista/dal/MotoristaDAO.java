package rblstudios.com.cadastromotorista.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import rblstudios.com.cadastromotorista.datatabase.DbHelper;
import rblstudios.com.cadastromotorista.interfaces.ICrud;
import rblstudios.com.cadastromotorista.model.Motorista;

/**
 * Criado por renan.lucas em 29/11/2017.
 */

public class MotoristaDAO implements ICrud<Motorista> {

    private DbHelper dbHelper;

    public MotoristaDAO(Context ctx) {
        this.dbHelper = new DbHelper(ctx);
    }

    public int inserir(Motorista motorista) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Motorista.NOME, motorista.getNome());
        valores.put(Motorista.IDADE, motorista.getIdade());
        valores.put(Motorista.TELEFONE, motorista.getTelefone());
        valores.put(Motorista.VALIDADE_CNH, motorista.getValidadeCNH());
        valores.put(Motorista.PLACA_VEICULO, motorista.getPlacaVeiculo());
        valores.put(Motorista.TIPO_VEICULO, motorista.getTipoVeiculo());
        valores.put(Motorista.VALOR_VEICULO, motorista.getValorVeiculo());
        valores.put(Motorista.CATEGORIA_MOTORISTA, motorista.getCategoriaMotorista());
        long id = db.insert(Motorista.TABLE, null, valores);

        db.close();
        return (int) id;
    }

    public void atualizar(Motorista motorista) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Motorista.NOME, motorista.getNome());
        valores.put(Motorista.IDADE, motorista.getIdade());
        valores.put(Motorista.TELEFONE, motorista.getTelefone());
        valores.put(Motorista.VALIDADE_CNH, motorista.getValidadeCNH());
        valores.put(Motorista.PLACA_VEICULO, motorista.getPlacaVeiculo());
        valores.put(Motorista.TIPO_VEICULO, motorista.getTipoVeiculo());
        valores.put(Motorista.VALOR_VEICULO, motorista.getValorVeiculo());
        valores.put(Motorista.CATEGORIA_MOTORISTA, motorista.getCategoriaMotorista());
        db.update(Motorista.TABLE, valores, Motorista.ID + " = ?", new String[]{String.valueOf(motorista.getId())});

        db.close();
    }

    public void excluir(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(Motorista.TABLE, Motorista.ID + " = ?", new String[]{String.valueOf(id)});
        db.close();
    }

    public List<Motorista> listarTudo() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String query = "SELECT "
                + Motorista.ID + ", "
                + Motorista.NOME + ", "
                + Motorista.IDADE + ", "
                + Motorista.TELEFONE + ", "
                + Motorista.VALIDADE_CNH + ", "
                + Motorista.PLACA_VEICULO + ", "
                + Motorista.TIPO_VEICULO + ", "
                + Motorista.VALOR_VEICULO + ", "
                + Motorista.CATEGORIA_MOTORISTA
                + " FROM " + Motorista.TABLE
                + " ORDER BY " + Motorista.NOME;

        List<Motorista> listMotorista = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, null);

        // Percorre todas as linhas encontradas na query
        if (cursor.moveToFirst()) {
            do {
                Motorista motorista = new Motorista();
                motorista.setId(cursor.getInt(cursor.getColumnIndex(Motorista.ID)));
                motorista.setNome(cursor.getString(cursor.getColumnIndex(Motorista.NOME)));
                motorista.setIdade(cursor.getInt(cursor.getColumnIndex(Motorista.IDADE)));
                motorista.setTelefone(cursor.getString(cursor.getColumnIndex(Motorista.TELEFONE)));
                motorista.setValidadeCNH(cursor.getString(cursor.getColumnIndex(Motorista.VALIDADE_CNH)));
                motorista.setPlacaVeiculo(cursor.getString(cursor.getColumnIndex(Motorista.PLACA_VEICULO)));
                motorista.setTipoVeiculo(cursor.getInt(cursor.getColumnIndex(Motorista.TIPO_VEICULO)));
                motorista.setValorVeiculo(cursor.getDouble(cursor.getColumnIndex(Motorista.VALOR_VEICULO)));
                motorista.setCategoriaMotorista(cursor.getInt(cursor.getColumnIndex(Motorista.CATEGORIA_MOTORISTA)));
                listMotorista.add(motorista);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return listMotorista;
    }

    public Motorista listarPorId(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String query = "SELECT "
                + Motorista.ID + ", "
                + Motorista.NOME + ", "
                + Motorista.IDADE + ", "
                + Motorista.TELEFONE + ", "
                + Motorista.VALIDADE_CNH + ", "
                + Motorista.PLACA_VEICULO + ", "
                + Motorista.TIPO_VEICULO + ", "
                + Motorista.VALOR_VEICULO + ", "
                + Motorista.CATEGORIA_MOTORISTA
                + " FROM " + Motorista.TABLE
                + " WHERE " + Motorista.ID + " = ?";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});

        Motorista motorista = new Motorista();
        // Percorre todas as linhas encontradas na query
        if (cursor.moveToFirst()) {
            do {
                motorista.setId(cursor.getInt(cursor.getColumnIndex(Motorista.ID)));
                motorista.setNome(cursor.getString(cursor.getColumnIndex(Motorista.NOME)));
                motorista.setIdade(cursor.getInt(cursor.getColumnIndex(Motorista.IDADE)));
                motorista.setTelefone(cursor.getString(cursor.getColumnIndex(Motorista.TELEFONE)));
                motorista.setValidadeCNH(cursor.getString(cursor.getColumnIndex(Motorista.VALIDADE_CNH)));
                motorista.setPlacaVeiculo(cursor.getString(cursor.getColumnIndex(Motorista.PLACA_VEICULO)));
                motorista.setTipoVeiculo(cursor.getInt(cursor.getColumnIndex(Motorista.TIPO_VEICULO)));
                motorista.setValorVeiculo(cursor.getDouble(cursor.getColumnIndex(Motorista.VALOR_VEICULO)));
                motorista.setCategoriaMotorista(cursor.getInt(cursor.getColumnIndex(Motorista.CATEGORIA_MOTORISTA)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return motorista;
    }
}
