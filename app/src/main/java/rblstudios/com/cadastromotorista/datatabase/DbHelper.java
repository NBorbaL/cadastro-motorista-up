package rblstudios.com.cadastromotorista.datatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import rblstudios.com.cadastromotorista.model.Motorista;

/**
 * Criado por renan.lucas on 06/11/2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "app.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        criaTabelaMotorista(db);
    }

    private void criaTabelaMotorista(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + Motorista.TABLE + "("
                + Motorista.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Motorista.NOME + " VARCHAR(150), "
                + Motorista.IDADE + " INTEGER, "
                + Motorista.TELEFONE + " VARCHAR(20), "
                + Motorista.VALIDADE_CNH + " VARCHAR(10), "
                + Motorista.PLACA_VEICULO + " VARCHAR(8), "
                + Motorista.TIPO_VEICULO + " INTEGER, "
                + Motorista.VALOR_VEICULO + " REAL, "
                + Motorista.CATEGORIA_MOTORISTA + " INTEGER" +
                ")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    // FUNÇÃO UTILIZADA POR "ANDROID DATABASE MANAGER" PARA VISUALIZAR BANCOS DE DADOS SQLITE, APAGAR EM PRODUÇÃO
    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"message"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }
}
