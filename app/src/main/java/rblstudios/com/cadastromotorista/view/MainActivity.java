package rblstudios.com.cadastromotorista.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import rblstudios.com.cadastromotorista.R;

public class MainActivity extends AppCompatActivity {

    private Button btnMotoristas;
    private TextView txtBancoDeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        encontrarViewsPorId();
        definirListenerBotoes();
    }

    private void encontrarViewsPorId() {
        btnMotoristas = findViewById(R.id.MainActivity_btnMotoristas);
        txtBancoDeDados = findViewById(R.id.MainActivity_txtBancoDeDados);
    }

    private void definirListenerBotoes() {
        btnMotoristas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListagemMotoristas.class);
                startActivity(intent);
            }
        });

        txtBancoDeDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AndroidDatabaseManager.class);
                startActivity(intent);
            }
        });
    }
}
