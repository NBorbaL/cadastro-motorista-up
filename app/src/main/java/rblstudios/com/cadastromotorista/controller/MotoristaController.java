package rblstudios.com.cadastromotorista.controller;

import android.content.Context;

import java.util.List;

import rblstudios.com.cadastromotorista.dal.MotoristaDAO;
import rblstudios.com.cadastromotorista.interfaces.ICrud;
import rblstudios.com.cadastromotorista.model.Motorista;
import rblstudios.com.cadastromotorista.util.ValidationUtil;

/**
 * Criado por renan.lucas 06/11/2017.
 */

public class MotoristaController implements ICrud<Motorista> {

    private MotoristaDAO motoristaDAO;

    public MotoristaController(Context ctx) {
        this.motoristaDAO = new MotoristaDAO(ctx);
    }

    @Override
    public int inserir(Motorista motorista) throws Exception {
        if (isModelValid(motorista)) {
            return motoristaDAO.inserir(motorista);
        }
        return -1;
    }

    @Override
    public void atualizar(Motorista motorista) throws Exception {
        if (isModelValid(motorista)) {
            motoristaDAO.atualizar(motorista);
        }
    }

    @Override
    public void excluir(int id) {
        motoristaDAO.excluir(id);
    }

    @Override
    public List<Motorista> listarTudo() {
        return motoristaDAO.listarTudo();
    }

    @Override
    public Motorista listarPorId(int id) {
        return motoristaDAO.listarPorId(id);
    }

    private boolean isModelValid(Motorista motorista) throws Exception {
        if (motorista.getNome() == null || motorista.getNome().trim().isEmpty()) {
            throw new Exception("Nome do motorista não pode ser vazio.");
        }

        if (!ValidationUtil.isAgeValid(motorista.getIdade())) {
            throw new Exception("Sua idade não se encaixa em nossos requerimentos. Sentimos muito.");
        }

        if (motorista.getTelefone() == null || !ValidationUtil.isTelefone(motorista.getTelefone())) {
            throw new Exception("Telefone inválido. Formato requerido: (00)00000-0000");
        }

        if (motorista.getValidadeCNH() != null && !motorista.getValidadeCNH().trim().isEmpty()) {
            if (!ValidationUtil.isValidDate(motorista.getValidadeCNH())) {
                throw new Exception("Data de validade inválida. Formato requerido: XX/XX/XXXX");
            } else if (!ValidationUtil.isValidCNH(motorista.getValidadeCNH())) {
                throw new Exception("Sua CNH deve ser válida por pelo menos mais um ano!");
            }
        } else {
            throw new Exception("Data de validade não pode ser vazia.");
        }

        if (motorista.getPlacaVeiculo() == null || !ValidationUtil.isValidPlaca(motorista.getPlacaVeiculo())) {
            throw new Exception("Placa inválida. Formato requerido: AAA-9999");
        }

        if (motorista.getTipoVeiculo() < 0) {
            throw new Exception("Tipo de veículo inválido.");
        }

        if (motorista.getValorVeiculo() < 1) {
            throw new Exception("Valor do veículo não pode ser menor que 1.");
        }

        if (motorista.getCategoriaMotorista() < 0) {
            throw new Exception("Categoria do motorista inválida.");
        }

        return true;
    }

    public void salvar(Motorista motorista) throws Exception {
        if (motorista.getId() > 0) {
            atualizar(motorista);
        } else {
            inserir(motorista);
        }
    }
}
