package rblstudios.com.cadastromotorista.model;

/**
 * Criado por renan.lucas em 06/11/2017.
 */

public class Motorista {

    public static final String TABLE = "MOTORISTA";

    public static final String ID = "ID";
    public static final String NOME = "NOME";
    public static final String IDADE = "IDADE";
    public static final String TELEFONE = "TELEFONE";
    public static final String VALIDADE_CNH = "VALIDADE_CNH";
    public static final String PLACA_VEICULO = "PLACA_VEICULO";
    public static final String TIPO_VEICULO = "TIPO_VEICULO";
    public static final String VALOR_VEICULO = "VALOR_VEICULO";
    public static final String CATEGORIA_MOTORISTA = "CATEGORIA_MOTORISTA";


    private int id;
    private String nome;
    private int idade;
    private String telefone;
    private String validadeCNH;
    private String placaVeiculo;
    private int tipoVeiculo;
    private double valorVeiculo;
    private int categoriaMotorista;

    public Motorista() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getValidadeCNH() {
        return validadeCNH;
    }

    public void setValidadeCNH(String validadeCNH) {
        this.validadeCNH = validadeCNH;
    }

    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    public int getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(int tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public double getValorVeiculo() {
        return valorVeiculo;
    }

    public void setValorVeiculo(double valorVeiculo) {
        this.valorVeiculo = valorVeiculo;
    }

    public int getCategoriaMotorista() {
        return categoriaMotorista;
    }

    public void setCategoriaMotorista(int categoriaMotorista) {
        this.categoriaMotorista = categoriaMotorista;
    }
}
